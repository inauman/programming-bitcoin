import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from helper import hash256
from S256Point import S256Point
from Signature import Signature

tx = '0100000001868278ed6ddfb6c1ed3ad5f8181eb0c7a38\
5aa0836f01d5e4789e6bd304d87221a000000475221022626e955ea6ea6d98850c994f9107b036\
b1334f18ca8830bfff1295d21cfdb702103b287eaf122eea69030a0e9feed096bed8045c8b98be\
c453e1ffac7fbdbd4bb7152aeffffffff04d3b11400000000001976a914904a49878c0adfc3aa0\
5de7afad2cc15f483a56a88ac7f400900000000001976a914418327e3f3dda4cf5b9089325a4b9\
5abdfa0334088ac722c0c00000000001976a914ba35042cfe9fc66fd35ac2224eebdafd1028ad2\
788acdc4ace020000000017a91474d691da1574e6b3c192ecfb52cc8984ee7b6c5687000000000\
1000000'

modified_tx = bytes.fromhex(tx)

s256 = hash256(modified_tx)

# sighash i.e. z in big endian format
z = int.from_bytes(s256, 'big')

print(hex(z))

# Validate Signature

# Grab SEC i.e. scriptPubKey from Redeem Script
sec = bytes.fromhex('022626e955ea6ea6d98850c994f9107b036b1334f18ca8830bfff1295d21cfdb70')

# Grab DER i.e. signature from ScriptSig
der = bytes.fromhex('3045022100dc92655fe37036f47756db8102e0d7d5e28b3beb83a8fef4f5dc0559bddfb94e02205a36d4e4e6c7fcd16658c50783e00c341609977aed3ad00937bf4ee942a89937')

point = S256Point.parse(sec)
print(point)

sig = Signature.parse(der)

print(point.verify(z, sig))