import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from helper import encode_base58_checksum

h160 = bytes.fromhex('74d691da1574e6b3c192ecfb52cc8984ee7b6c56')

# p2pkh mainnet - 0x00 --> address start with 1
print(encode_base58_checksum(b'\x00' + h160))

# p2pkh testnet - 0x6f --> address start with m
print(encode_base58_checksum(b'\x6f' + h160))

# p2sh mainnet - 0x05 --> address start with 3
print(encode_base58_checksum(b'\x05' + h160))

# p2sh testnet - 0xc4 --> address start with 2
print(encode_base58_checksum(b'\xc4' + h160))
