# https://github.com/bitcoin/bitcoin/tree/master/doc

# Bitcoin Core Development on MacOS — A Step by Step Guide
https://medium.com/coincorner/bitcoin-core-development-on-macos-a-step-by-step-guide-5ecf8b17eb49

# .gitignore
https://github.com/github/gitignore

# Bitcoin wiki
https://en.bitcoin.it/wiki/Main_Page
s
# Learning Cryptography, Part 1: Finite Fields
https://medium.com/loopring-protocol/learning-cryptography-finite-fields-ced3574a53fe

# Elliptic Curve
Online Graphing Equations
https://www.desmos.com/calculator

# Bitwise Operators in Python:
https://realpython.com/python-bitwise-operators

# Overriding multiplication using __mul__ and __rmul__
https://www.geeksforgeeks.org/__rmul__-in-python/

# Understanding Big and Little Endian Byte Order
https://betterexplained.com/articles/understanding-big-and-little-endian-byte-order/
https://www.researchgate.net/post/Why_we_are_using_HEXADECIMAL_values_for_computer_addressing

# Testnet
Faucet: 
    https://coinfaucet.eu/en/btc-testnet/   *****
    https://testnet-faucet.mempool.co/       ***
Explorer: https://blockstream.info/testnet


# Tutorial: Setup Test Node
https://gist.github.com/System-Glitch/cb4e87bf1ae3fec9925725bb3ebe223a
https://bitcoin.org/en/full-node#configuring-dhcp
https://www.ulam.io/blog/how-to-setup-a-custom-bitcoin-testnet/


# Setup
Conf file:      /Users/nauman/Library/Application\ Support/Bitcoin/bitcoin.conf
Data Directory: /Users/nauman/bitcoin/data/regtest/wallets

bitcoind >> ~/Downloads/logs1.txt
bitcoin-cli stop
bitcoin-cli -regtest createwallet "wallet1"
bitcoin-cli -regtest loadwallet "wallet1"

# https://en.bitcoin.it/wiki/Protocol_documentation
Network	            Magic value	        Sent over wire as
main	            0xD9B4BEF9	        F9 BE B4 D9
testnet/regtest	    0xDAB5BFFA	        FA BF B5 DA
testnet3	        0x0709110B	        0B 11 09 07
signet(default)	    0x40CF030A	        0A 03 CF 40
namecoin	        0xFEB4BEF9	        F9 BE B4 FE