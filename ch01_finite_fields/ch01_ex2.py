primes = [7, 11, 17, 31]

for p in primes:
    #print([ (i ** (p-1)) % p for i in range(1,p)])
    print([pow(i, p-1, p) for i in range(1,p)])
        