import sys
sys.append('/Users/nauman/Projects/programming-bitcoin/')
from helper import hash256, hash160

def op_dup(stack):
    if len(stack) < 1:
        return False
    stack.append(stack[:-1])
    return True

def op_hash256(stack):
    if len(stack) < 1:
        return False
    element = stack.pop()
    stack.append(hash256(element))
    return True

def op_hash160(stack):
    if len(stack) < 1:
        return False
    element = stack.pop()
    stack.append(hash160(element))
    return True