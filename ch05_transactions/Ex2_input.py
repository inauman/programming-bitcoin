import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from io import BytesIO
from helper import little_endian_to_int, read_varint
from script import Script

# Raw hex of the transation
raw_txn_hex = '0100000001813f79011acb80925dfe69b3def355fe914bd1d96a3\
f5f71bf8303c6a989c7d1000000006b483045022100ed81ff192e75a3fd2304004dca\
db746fa5e24c5031ccfcf21320b0277457c98f02207a986d955c6e0cb35d446a89d3f5\
6100f4d7f67801c31967743a9c8e10615bed01210349fc4e631e3624a545de3f89f5d86\
84c7b8138bd94bdd531d2e213bf016b278afeffffff02a135ef01000000001976a914bc\
3b654dca7e56b04dca18f2566cdaf02e8d9ada88ac99c39800000000001976a9141c4bc\
762dd5423e332166702cb75f40df79fea1288ac19430600'

# Convert hex txn to bytes
raw_txn_bytes = bytes.fromhex(raw_txn_hex)

# Get the buffered txn stream
stream = BytesIO(raw_txn_bytes)

# Read the first 4 bytes for the version number in little endian format
version_little_endian = stream.read(4)

# Convert little endian to int
version = little_endian_to_int(version_little_endian)

print(f'Version: {version}\n')

# Number of inputs
num_of_inputs = read_varint(stream)

print(f'Number of inputs: {num_of_inputs}\n')

# Input
# Previous Txn Id: 32 bytes
# Reverse the order to change from little endian
prev_txn_id = stream.read(32)[::-1]
print(f'Previous Txn Id: {prev_txn_id}\n')

# Previous txn index i.e. which output to spend
prev_txn_index = little_endian_to_int(stream.read(4)) 
print(f'Previous Txn Index: {prev_txn_index}\n')

# Script
script_sig = Script.parse(stream)
print(f'Script:\n\t{script_sig}\n')

# Sequence: used for RBF (Replace By Fee) and OP_CHECKSEQUENCEVERIFY
sequence = little_endian_to_int(stream.read(4))
print(f'Sequence: {sequence}')


