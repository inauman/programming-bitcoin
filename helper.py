import unittest
from unittest import TestCase, TestSuite, TextTestRunner

import hashlib


SIGHASH_ALL = 1
SIGHASH_NONE = 2
SIGHASH_SINGLE = 3
BASE58_ALPHABET = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'
TWO_WEEKS = 60 * 60 * 24 * 14
MAX_TARGET = 0xffff * 256**(0x1d - 3)

def run(test):
    suite = TestSuite()
    suite.addTest(test)
    TextTestRunner().run(suite)


def hash160(s):
    '''sha256 followed by ripemd160'''
    return hashlib.new('ripemd160', hashlib.sha256(s).digest()).digest()


def hash256(s):
    '''two rounds of sha256'''
    return hashlib.sha256(hashlib.sha256(s).digest()).digest()


def encode_base58(s):
    # determine how many 0 bytes (b'\x00') s starts with
    count = 0
    for c in s:
        if c == 0:
            count += 1
        else:
            break
    # convert to big endian integer
    num = int.from_bytes(s, 'big')
    prefix = '1' * count
    result = ''
    while num > 0:
        num, mod = divmod(num, 58)
        result = BASE58_ALPHABET[mod] + result
    return prefix + result

def encode_base58_checksum(s):
    return encode_base58(s + hash256(s)[:4])


def decode_base58(s):
    num = 0
    for c in s:
        num *= 58
        num += BASE58_ALPHABET.index(c)
    combined = num.to_bytes(25, byteorder='big')
    checksum = combined[-4:]
    if hash256(combined[:-4])[:4] != checksum:
        raise ValueError('bad address: {} {}'.format(checksum, hash256(combined[:-4])[:4]))
    return combined[1:-4]


def little_endian_to_int(b):
    '''little_endian_to_int takes byte sequence as a little-endian number.
    Returns an integer'''
    return int.from_bytes(b, 'little')


def int_to_little_endian(n, length):
    '''endian_to_little_endian takes an integer and returns the little-endian
    byte sequence of length'''
    return n.to_bytes(length, 'little')


def read_varint(s):
    ''' Reads a variable integer from a stream '''
    i = s.read(1)[0]
    
    if i == 0xfd:
        # 0xfd => next two bytes are the number
        return little_endian_to_int(s.read(2))
    elif i == 0xfe:
        # 0xfe => next four bytes are the number
        return little_endian_to_int(s.read(4))
    elif i == 0xff:
        # 0xff => next eight bytes are the number
        return little_endian_to_int(s.read(8))
    else:
        # anything else is just the 1 byte integer < 255
        return i
    
def encode_varint(i):
    ''' Encode an integer as a var int '''
    if i < 0xfd:
        return bytes([i])
    elif i < 0x10000:
        return b'\xfd' + int_to_little_endian(i, 2)
    elif i < 0x100000000:
        return b'\xfe' + int_to_little_endian(i, 4)
    elif i < 0x10000000000000000:
        return b'\xff' + int_to_little_endian(i, 8)
    else:
        raise ValueError(f'integer too large: {i}')

def h160_to_p2pkh_address(h160, testnet=False):
    if testnet:
        # p2pkh testnet - 0x6f --> address start with m
        prefix = b'\x6f'
    else:
        # p2pkh mainnet - 0x00 --> address start with 1
        prefix = b'\x00'
    return encode_base58_checksum(prefix + h160)

def h160_to_p2sh_address(h160, testnet=False):
    if testnet:
        # p2sh testnet - 0xc4 --> address start with 2
        prefix = b'\xc4'
    else:
        # p2sh mainnet - 0x05 --> address start with 3
        prefix = b'\x05'
    return encode_base58_checksum(prefix + h160)

def bits_to_target(bits):
    # last bit is exponent
    exponent = bits[-1]
    
    # coefficient is the first 3 bits in LE format
    coefficient = little_endian_to_int(bits[:-1])
    
    # target = coefficient * 256**(exponent - 3)
    target = coefficient * 256**(exponent - 3)

    return target

def target_to_bits(target):
    '''Turns a target integer back into bits'''
    raw_bytes = target.to_bytes(32, 'big')
    raw_bytes = raw_bytes.lstrip(b'\x00')
    
    if raw_bytes[0] > 0x7f:
        exponent = len(raw_bytes) + 1
        coefficient = b'\x00' + raw_bytes[:2]
    else:
        exponent = len(raw_bytes)
        coefficient = raw_bytes[:3]
    
    new_bits = coefficient[::-1] + bytes([exponent])
    return new_bits

def calculate_new_bits(previous_bits, time_differential):
    previous_target = bits_to_target(previous_bits)
    
    if time_differential > TWO_WEEKS * 4:
        time_differential = TWO_WEEKS * 4
    
    if time_differential < TWO_WEEKS // 4:
        time_differential = TWO_WEEKS // 4
    
    new_target = previous_target * time_differential // TWO_WEEKS
    
    if new_target > MAX_TARGET:
        new_target = MAX_TARGET
        
    return target_to_bits(new_target)

def merkle_parent(hash1, hash2):
    return hash256(hash1 + hash2)

def merkle_parent_level(hashes):
    '''Takes a list of binary hashes and returns the merkle root
    '''
    n = num_leaves = len(hashes)

    if num_leaves == 1:
        raise RuntimeError('Only 1 element, nothing to hash here')
        #return hashes[n - 1]

    # If list has odd number of hashes, duplicate the last one to get an even list to hash in pairs
    if num_leaves % 2 != 0:
        hashes.append(hashes[n - 1])
    
    new_hash_list = []

    # Hash the hashes in pair
    for i in range(0, num_leaves, 2):
        new_hash_list.append(hash256(hashes[i] + hashes[i+1]))
    
    return new_hash_list

def merkle_root(hashes):
    # current level starts as hashes
    # loop until there's exactly 1 element
    while len(hashes) != 1:
        # current level becomes the merkle parent level
        hashes = merkle_parent_level(hashes)
    
    # return the 1st item of the current level 
    return hashes[0]      

def bytes_to_bit_field(some_bytes):
    flag_bits = []
    for byte in some_bytes:
        for _ in range(8):
            flag_bits.append(byte & 1)
            byte >>= 1
        return flag_bits
    
    
class HelperTest(TestCase):

    def test_little_endian_to_int(self):
        h = bytes.fromhex('99c3980000000000')
        want = 10011545
        self.assertEqual(little_endian_to_int(h), want)
        h = bytes.fromhex('a135ef0100000000')
        want = 32454049
        self.assertEqual(little_endian_to_int(h), want)

    def test_int_to_little_endian(self):
        n = 1
        want = b'\x01\x00\x00\x00'
        self.assertEqual(int_to_little_endian(n, 4), want)
        n = 10011545
        want = b'\x99\xc3\x98\x00\x00\x00\x00\x00'
        self.assertEqual(int_to_little_endian(n, 8), want)

    def test_decode_base58(self):
        want = b"P{'A\x1c\xcf\x7f\x16\xf1\x02\x97\xdel\xef?)\x16#\xed\xdf"
        self.assertEqual(decode_base58('mnrVtF8DWjMu839VW3rBfgYaAfKk8983Xf'), want)
if __name__ == '__main__':
    unittest.main()
    