import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from FieldElement import FieldElement
from Point import Point

prime = 223
a = FieldElement(num=0, prime=prime)
b = FieldElement(num=7, prime=prime)

points_to_be_added = [((192, 105),(17, 56)),((170, 142), (60, 139)),((47, 71),(17, 56)),((143,98),(76,66))]

for t1, t2 in points_to_be_added:
    x1_raw, y1_raw = t1  
    x2_raw, y2_raw = t2  
    x1 = FieldElement(num=x1_raw, prime=prime)
    y1 = FieldElement(num=y1_raw, prime=prime)
    x2 = FieldElement(num=x2_raw, prime=prime)
    y2 = FieldElement(num=y2_raw, prime=prime)

    p1 = Point(x1, y1, a, b)
    p2 = Point(x2, y2, a, b)
    print(p1 + p2)

