import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from FieldElement import FieldElement
from Point import Point

# EC y^2 = x^3 + 7

prime = 223
a_raw, b_raw = (0, 7)
x_raw, y_raw = (15, 86)
a = FieldElement(a_raw, prime)
b = FieldElement(b_raw, prime)
x = FieldElement(x_raw, prime)
y = FieldElement(y_raw, prime)

p = Point(x, y, a, b)
inf = Point(None, None, a, b)

counter = 1
product = p
print(f'{counter} - {product}')
while product != inf:
    product += p
    counter += 1
    print(f'{counter} - {product}')
print(f'Order of group is {counter} which would result in Identity')
