import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')
from S256Point import S256Point, G, N
from Signature import Signature

from helper import hash256
 
# Shhhh...It's a private key
#e = 12345
e = int.from_bytes(hash256(b'12345'), 'big')

# Signature hash i.e. double hash of the message using sha256
z = int.from_bytes(hash256(b'Programming Bitoin!'), 'big')

# Random number
k = 1234567890

# r of signature(r, s)
r = (k*G).x.num

k_inv = pow(k, N-2, N)

# s of signature(r, s)
s = (z + r * e) * k_inv % N

point = e * G

sig = Signature(r, s)

print(point)

print(sig)

print(hex(z))

print(hex(r))

print(hex(s))