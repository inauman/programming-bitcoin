# Elliptic Curve y**2 = x ** 3 + 7 over Finite Field F223
# (192, 105), (17, 56), (200, 119), (1, 193), (42, 99)
FP = 223
points = [(192, 105), (17, 56), (200, 119), (1, 193), (42, 99)]

for x,y in points:
    lh = (y ** 2) % FP
    rh = (x ** 3 + 7) % FP
    
    if lh == rh:
        print(f"({x}, {y}) is on the curve")
    else:
        print(f"({x}, {y}) is NOT on the curve")
