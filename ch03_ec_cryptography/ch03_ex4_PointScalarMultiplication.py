import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from FieldElement import FieldElement
from Point import Point

prime = 223
a = FieldElement(num=0, prime=prime)
b = FieldElement(num=7, prime=prime)
x_raw, y_raw = (47, 71)
x = FieldElement(x_raw, prime)
y = FieldElement(y_raw, prime)

p = Point(x, y, a, b)

'''print(p)    
print(p+p)
print(p+p+p+p)
print(p+p+p+p+p+p+p+p)
print(p+p+p+p+p+p+p+p+p+p+p+p+p+p+p+p+p+p+p+p+p)'''

for s in range(1,22):
    result = s * p
    print(f'{s} - {result}')
    
  
