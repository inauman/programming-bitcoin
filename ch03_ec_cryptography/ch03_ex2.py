# Elliptic Curve y**2 = x ** 3 + 7 over Finite Field F223
# (192, 105), (17, 56), (200, 119), (1, 193), (42, 99)
import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from FieldElement import FieldElement
from Point import Point

prime = 223
points = [(192, 105), (17, 56), (200, 119), (1, 193), (42, 99)]

a = FieldElement(num=0, prime=223)
b = FieldElement(num=7, prime=223)


for x_raw, y_raw in points:
    try:
        x = FieldElement(num=x_raw, prime=223)
        y = FieldElement(num=y_raw, prime=223)
        p = Point(x, y, a, b)
        print(p)
    except ValueError:
        print(f'({x_raw}, {y_raw}) is not on the curve')