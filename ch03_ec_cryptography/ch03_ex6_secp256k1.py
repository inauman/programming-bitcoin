import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from FieldElement import FieldElement
from Point import Point

gx = 0x79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798
gy = 0x483ada7726a3c4655da4fbfc0e1108a8fd17b448a68554199c47d08ffb10d4b8
p = 2**256 - 2**32 - 977
n = 0xfffffffffffffffffffffffffffffffebaaedce6af48a03bbfd25e8cd0364141

# Verify the point G(gx, gy) is on the curve i.e. (y**2)%p = (x**3 + 7)%p 
print(gy**2%p == (gx**3+7)%p)

# Verify the order of the Group G is n i.e. n * G = I

x = FieldElement(gx, p)
y = FieldElement(gy, p)
a = FieldElement(0, p)
b = FieldElement(7, p)

G = Point(x, y, a, b)
print(n*G)