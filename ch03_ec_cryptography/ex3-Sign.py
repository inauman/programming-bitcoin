import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from S256Point import S256Point, G, N
from helper import hash256

# Shhhh....It is a private key: e
e = int.from_bytes(hash256(b'my secret'),'big')

# Create the signature hash i.e. hash of the message: z
z = int.from_bytes(hash256(b'my message'), 'big')

# Choose a random number: k
k = 1234567890

# r is the x coordinate of the point on elliptic curve
r = (k*G).x.num

# 1/k using Fermat's little theorem
k_inv = pow(k, N-2, N)

# Compute s
s = (z + r * e) * k_inv % N

point = e * G

print(point)

print(hex(z))

print(hex(r))

print(hex(s))