import unittest
import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from app.ch01_finite_fields.FieldElement import FieldElement
from app.ch02_elliptic_curves.Point import Point

class ECCTest(unittest.TestCase):
    def test_on_curve(self):
        prime = 223
        a= FieldElement(0, prime)
        b = FieldElement(7, prime)
        valid_points = ((192,105), (17, 56), (1, 193))
        invalid_points = ((200, 199), (42, 99))
        for x_raw, y_raw in valid_points:
            x = FieldElement(x_raw, prime)
            y = FieldElement(y_raw, prime)
            Point(x, y, a, b)
        for x_raw, y_raw in invalid_points:
            x = FieldElement(x_raw, prime)
            y = FieldElement(y_raw, prime)
            with self.assertRaises(ValueError):
                Point(x, y, a, b)
    
    def test_add(self):
        prime = 223
        a = FieldElement(num=0, prime=prime)
        b = FieldElement(num=7, prime=prime)

        points_to_be_added = [((192, 105),(17, 56))]

        for t1, t2 in points_to_be_added:
            x1_raw, y1_raw = t1  
            x2_raw, y2_raw = t2  
            x1 = FieldElement(num=x1_raw, prime=prime)
            y1 = FieldElement(num=y1_raw, prime=prime)
            x2 = FieldElement(num=x2_raw, prime=prime)
            y2 = FieldElement(num=y2_raw, prime=prime)

            p1 = Point(x1, y1, a, b)
            p2 = Point(x2, y2, a, b)
            
            #expected point on the EC over F223
            p3 = Point(FieldElement(170, prime), FieldElement(142, prime), a, b)
            
            print(p1 + p2 == p3)
            
if __name__ == "__main__":
    unittest.main()