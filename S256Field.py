from FieldElement import FieldElement

# secp256k1 parameters
A=0
B=7
N = 0xfffffffffffffffffffffffffffffffebaaedce6af48a03bbfd25e8cd0364141
P = 2**256 - 2**32 - 977

class S256Field(FieldElement):
    def __init__(self, num, prime=None):
        super().__init__(num=num, prime=P)
    
    def sqrt(self):
        # // floor division
        return self**((P + 1) // 4)
    
    # hexadecimal representation of the number
    # with filled zeros   
    def __repr__(self):
        return f'{self.num:x}'.zfill(64)

