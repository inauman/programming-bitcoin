import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from io import BytesIO
from helper import little_endian_to_int, int_to_little_endian, hash256, bits_to_target, merkle_root

GENESIS_BLOCK = bytes.fromhex('0100000000000000000000000000000000000000000000000000000000000000000000003ba3edfd7a7b12b27ac72c3e67768f617fc81bc3888a51323a9fb8aa4b1e5e4a29ab5f49ffff001d1dac2b7c')
TESTNET_GENESIS_BLOCK = bytes.fromhex('0100000000000000000000000000000000000000000000000000000000000000000000003ba3edfd7a7b12b27ac72c3e67768f617fc81bc3888a51323a9fb8aa4b1e5e4adae5494dffff001d1aa4ae18')
LOWEST_BITS = bytes.fromhex('ffff001d')


class Block:
    def __init__(self, version, prev_block, merkle_root, timestamp, bits, nonce, tx_hashes=None):
        self.version = version
        self.prev_block = prev_block
        self.merkle_root = merkle_root
        self.timestamp = timestamp
        self.bits = bits
        self.nonce = nonce
        self.tx_hashes = tx_hashes
    
    @classmethod    
    def parse(cls, s, testnet=False):
        '''Takes a byte stream and parses a block. Returns a Block object'''
        # s.read(n) will read n bytes from the stream
        # block is fixwidth 80 bytes
    
        # version - 4 bytes, little endian, interpret as int
        version = little_endian_to_int(s.read(4))

        # prev_block - 32 bytes, little endian (use [::-1] to reverse)
        prev_block = s.read(32)[::-1]
  
        # merkle_root - 32 bytes, little endian (use [::-1] to reverse)
        merkle_root = s.read(32)[::-1]
        
        # timestamp - 4 bytes, little endian, interpret as int
        timestamp = little_endian_to_int(s.read(4))
        
        # bits - 4 bytes
        bits = s.read(4)
        
        # nonce - 4 bytes
        nonce = s.read(4)
        
        return cls(version=version, prev_block=prev_block, merkle_root=merkle_root, timestamp=timestamp, bits=bits, nonce=nonce)
    
    def serialize(self):
        '''Returns the 80 byte block header'''
        
        # version - 4 bytes, change from int to little endian
        result = int_to_little_endian(self.version, 4)
        
        # prev_block - 32 bytes, change to little endian
        result += self.prev_block[::-1]
        
        # merkle root - 32 bytes, change to little endian
        result += self.merkle_root[::-1]
        
        # timestamp - 4 bytes, change from int to little endian
        result += int_to_little_endian(self.timestamp, 4)
        
        # bits - bytes
        result += self.bits
        
        # nonce - 4 bytes
        result += self.nonce
        
        return result
    
    def hash(self):
        '''Returns the hash256 interpreted little endian of the block'''
        
        # Serialize
        s = self.serialize()
        
        # Hash256
        sha = hash256(s)
        
        # Reverse
        return sha[::-1]
        
    def bip9(self):
        return self.version >> 29 == 0b001
        
    def bip91(self):
        return self.version >> 4 & 1 == 1

    def bip141(self):
        return self.version >> 1 & 1 == 1

    def target(self):
        '''Returns the proof-of-work target based on the bits'''
        return bits_to_target(self.bits)
    
    # difficulty is inversely proportional of target
    # difficulty = 0xffff x 256**(0x1d-3)/target
    def difficulty(self):
        lowest = 0xffff * 256**(0x1d - 3)
        return lowest / self.target()
    
    def check_pow(self):
        '''Returns whether this block satisfies proof of work'''
        # get the hash256 of the serialization of this block
        h256 = hash256(self.serialize())
        # interpret this hash as a little-endian number
        proof = little_endian_to_int(h256)
        # return whether this integer is less than the target
        return proof < self.target()

    def validate_merkle_root(self):
        '''Gets the merkle root of the tx_hashes and checks that it's
        the same as the merkle root of this block.
        '''
        # LE => reverse each item in self.tx_hashes
        rev_tx_hashes = [x[::-1] for x in self.tx_hashes]
        
        # compute the Merkle Root and reverse => LE
        root = merkle_root(rev_tx_hashes)[::-1]

        # return whether self.merkle_root is the same
        return self.merkle_root == root