import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from network import SimpleNode, VersionMessage, VerAckMessage

# Regtest
node = SimpleNode('127.0.0.1', testnet=True, logging=True)

# Testnet 3
# node = SimpleNode('testnet.programmingbitcoin.com', testnet=True, logging=True)

version = VersionMessage()
node.send(version)
verack_received = False
version_received = False

while not verack_received and not version_received:
    message = node.wait_for(VersionMessage, VerAckMessage)
    if message.command == VerAckMessage.command:
        verack_received = True
    else:
        version_received = True
        #node.send(VerAckMessage())