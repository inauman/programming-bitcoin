import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from Block import Block, GENESIS_BLOCK, LOWEST_BITS
from network import SimpleNode, GetHeadersMessage, HeadersMessage
from helper import calculate_new_bits

from io import BytesIO

node = SimpleNode('mainnet.programmingbitcoin.com', testnet=False)
node.handshake()
genesis = Block.parse(BytesIO(GENESIS_BLOCK))

getheaders = GetHeadersMessage(start_block=genesis.hash())

node.send(getheaders)