import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from network import NetworkEnvelope, VersionMessage
import socket


# https://github.com/jimmysong/programmingbitcoin/issues/151
#host = 'testnet.programmingbitcoin.com'

# Regtest
host = '127.0.0.1'
port = 18444

socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.connect((host, port))

stream = socket.makefile('rb', None)
version = VersionMessage()

envelope = NetworkEnvelope(version.command, version.serialize(), testnet=True)
#print(envelope)
socket.sendall(envelope.serialize())

while True:
    new_message = NetworkEnvelope.parse(stream, testnet=True)
    print(new_message)
    break
