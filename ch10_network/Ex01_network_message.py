import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from network import NetworkEnvelope
from io import BytesIO

network_message = 'f9beb4d976657261636b000000000000000000005df6e0e2'

s = BytesIO(bytes.fromhex(network_message))

network_envlope = NetworkEnvelope.parse(s)

print(network_envlope.command)

print(network_envlope.payload)

print(network_envlope.magic)