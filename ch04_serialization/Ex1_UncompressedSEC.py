import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from PrivateKey import PrivateKey
from S256Point import S256Point, G, N

# Private Key
e = PrivateKey(5000)

print(e.point.sec(compressed=False).hex())

# Private Key
e = PrivateKey(2018**5)

print(e.point.sec(compressed=False).hex())

# Private Key
e = PrivateKey(0xdeadbeef12345)

print(e.point.sec(compressed=False).hex())