def int_to_little_endian(numint, length):
    return int.to_bytes(numint,length,'little')

def int_to_big_endian(numint, length):
    return int.to_bytes(numint,length,'big')

# little endian 1 printed in 8 bytes
# b'\x01\x00\x00\x00\x00\x00\x00\x00'
n=1
print(int_to_little_endian(n, 8))

# big endian 1 printed in 8 bytes
# b'\x00\x00\x00\x00\x00\x00\x00\x01'
n=1
print(int_to_big_endian(n, 8))

# little endian 10011545 printed in 8 bytes
# b'\x99\xc3\x98\x00\x00\x00\x00\x00'
n = 10011545
print(int_to_little_endian(n, 8))

# big endian 10011545 printed in 8 bytes
# b'\x00\x00\x00\x00\x00\x98\xc3\x99'
n = 10011545
print(int_to_big_endian(n, 8))


