import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from PrivateKey import PrivateKey
from S256Point import S256Point, G, N

e = PrivateKey(5001)
print(e.point.sec().hex())

e = PrivateKey(2019**5)
print(e.point.sec().hex())
              
e = PrivateKey(0xdeadbeef54321)
print(e.point.sec().hex())