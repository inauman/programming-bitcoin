
    
def little_endian_to_int(numbytes):        
    return int.from_bytes(numbytes, 'little')

def big_endian_to_int(numbytes):        
    return int.from_bytes(numbytes, 'big')

hexstr = '99c3980000000000'
bytesstr = bytes.fromhex(hexstr)
print(bytesstr)
num = little_endian_to_int(bytesstr)
print(num)
print("*****************************************")
num = big_endian_to_int(bytesstr)
print(num)
print("=========================================")

hexstr = 'a135ef0100000000'
bytesstr = bytes.fromhex(hexstr)
print(bytesstr)
num = little_endian_to_int(bytesstr)
print(num)
print("*****************************************")
num = big_endian_to_int(bytesstr)
print(num)
print("=========================================")