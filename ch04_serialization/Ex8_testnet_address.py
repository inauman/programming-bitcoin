import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from PrivateKey import PrivateKey
from helper import hash256, little_endian_to_int

# Secret seed
secret = input("Please enter your secret: ")

# Encode the secret in binary format
passpharase = secret.encode()

# run it through double round of hashing
digest = hash256(passpharase)

# converet it into little endian
little_n = little_endian_to_int(digest) 

# Get the private key
e = PrivateKey(little_n)

# Testnet address
print(f'Testnet Address: {e.point.address(compressed=True, testnet=True)}')

'''
    # shhh...it is a secret md.nauman@gmail.com!
    # Address mgWFy3V19Unq7HBwd46R8WJQgrrraxQ3cr
    # Use the following faucet to sent tbtc
    # https://testnet-faucet.mempool.co/
    # Use the following testnet explorer to verify
    # https://blockstream.info/testnet
    # Successfully sent 0.001 tbtc to mgWFy3V19Unq7HBwd46R8WJQgrrraxQ3cr on 07/16/2021
    # https://blockstream.info/testnet/address/mgWFy3V19Unq7HBwd46R8WJQgrrraxQ3cr
    # TxId cda80b9607b85bcfb2e9116da57f4dc0d7a5235e65fc276930bac697d1bb5786
'''