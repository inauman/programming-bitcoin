import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')
from PrivateKey import PrivateKey

e = PrivateKey(5002)

print(e.point.address(compressed=False, testnet=True))

e = PrivateKey(2020**5)
print(e.point.address(compressed=True, testnet=True))

e = PrivateKey(0x12345deadbeef)
print(e.point.address(compressed=True, testnet=False))