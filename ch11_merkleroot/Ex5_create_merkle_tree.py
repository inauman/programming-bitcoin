import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

import math

# number of leaves in the merkle tree
total = 27

# Depth of the tree

max_depth = math.ceil(math.log(total, 2))
print(f'max_depth: {max_depth}')

merkle_tree = []

for depth in range(max_depth + 1):
    num_items = math.ceil(total / 2**(max_depth - depth))
    level_hashes = [None] * num_items
    merkle_tree.append(level_hashes)

for i,level in enumerate(merkle_tree):
    print(f'level{i} --> {level}')