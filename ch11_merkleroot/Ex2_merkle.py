import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from helper import hash256, merkle_parent_level

hex_hashes = [
'c117ea8ec828342f4dfb0ad6bd140e03a50720ece40169ee38bdc15d9eb64cf5',
'c131474164b412e3406696da1ee20ab0fc9bf41c8f05fa8ceea7a08d672d7cc5',
'f391da6ecfeed1814efae39e7fcb3838ae0b02c02ae7d0a5848a66947c0727b0',
'3d238a92a94532b946c90e19c49351c763696cff3db400485b813aecb8a13181',
'10092f2633be5f3ce349bf9ddbde36caa3dd10dfa0ec8106bce23acbff637dae']

hashes = [bytes.fromhex(x) for x in hex_hashes]

hex_hashes = [x.hex() for x in merkle_parent_level(hashes)]

print(hex_hashes)