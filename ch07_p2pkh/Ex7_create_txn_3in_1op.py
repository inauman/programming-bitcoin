import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from PrivateKey import PrivateKey
from Script import Script, p2pkh_script
from helper import SIGHASH_ALL, hash256, decode_base58, little_endian_to_int
from Tx import Tx, TxIn, TxOut

tx_ins = []

# Input Tx 1 From faucet 
prev_tx = bytes.fromhex('382af48728d049720513640faac1e2ecc9708354efa54044d36b33753f2c79bc')
prev_index = 1
tx_ins.append(TxIn(prev_tx, prev_index))

# Input Tx 2 From faucet 
prev_tx = bytes.fromhex('470a5fb78d72786ed34fc6b6ef72f1d15349fb9d9805027defe95149bb839408')
prev_index = 0
tx_ins.append(TxIn(prev_tx, prev_index))

# Input Tx 3 From previous txn
prev_tx = bytes.fromhex('b4a72166dffebfc6ba080ae8d8e2b888eee03c8b1873fa9d408b821d34caabd5')
prev_index = 0
tx_ins.append(TxIn(prev_tx, prev_index))

# Private Key - e
secret = 'shhh...it is a secret md.nauman@gmail.com!'
passpharase = secret.encode()
digest = hash256(passpharase)
little_n = little_endian_to_int(digest)
e = PrivateKey(little_n)

# Output Tx - My own testnet address
# total input amount (of all 3 in tx) is 1411631
# pay 485 sats in fee
# target amount is 1411146
# In 1 = 1271931
# In 2 =  100000
# In 3 =   39700
# ================
# Total  1411631
# ================
# Fee        485
# ================
# Target 1411146

tx_outs = []
target_address = 'mgWFy3V19Unq7HBwd46R8WJQgrrraxQ3cr'
h160 = decode_base58(target_address)
script_pubkey = p2pkh_script(h160)
balance = 2670039
fee = 330
target_amount = balance - fee
tx_outs.append(TxOut(target_amount, script_pubkey))

# Create Tx
tx_obj = Tx(1, tx_ins, tx_outs, 0, testnet=True)

print(tx_obj.sign_input(0, e))
print(tx_obj.sign_input(1, e))
print(tx_obj.sign_input(2, e))

print(tx_obj.serialize().hex())

'''
shhh...it is a secret md.nauman@gmail.com!
'''
