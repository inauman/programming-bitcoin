import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from Signature import Signature
from S256Point import S256Point

# SEC Public key
sec = bytes.fromhex('0349fc4e631e3624a545de3f89f5d8684c7b8138bd94bdd531d2e213bf016b278a')

# DER Signature
der = bytes.fromhex('3045022100ed81ff192e75a3fd2304004dcadb746fa5e24c5031c\
cfcf21320b0277457c98f02207a986d955c6e0cb35d446a89d3f56100f4d7f67801c31967743a9\
c8e10615bed')

# hash of the message i.e. signature hash
z = 0x27e0c5994dec7824e56dec6b2fcb342eb7cdb0d0957c2fce9882f715e85d81a6

point = S256Point.parse(sec)

signature = Signature.parse(der)

# Verify the sinature for each input message
print(point.verify(z, signature))
