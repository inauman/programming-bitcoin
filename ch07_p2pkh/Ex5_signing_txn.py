import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from PrivateKey import PrivateKey
from helper import decode_base58, SIGHASH_ALL
from Script import Script, p2pkh_script
from Tx import TxIn, TxOut, Tx

# Part 1: Creating the Transaction
prev_tx = bytes.fromhex('0d6fe5213c0b3291f208cba8bfb59b7476dffacc4e5cb66f6\
eb20a080843a299')

prev_index = 13

tx_in = TxIn(prev_tx, prev_index)

tx_outs = []

change_amount = int(0.33*100000000)

change_h160 = decode_base58('mzx5YhAH9kNHtcN481u6WkjeHjYtVeKVh2')

change_script = p2pkh_script(change_h160)

change_output = TxOut(amount=change_amount, script_pubkey=change_script)

target_amount = int(0.1*100000000)

target_h160 = decode_base58('mnrVtF8DWjMu839VW3rBfgYaAfKk8983Xf')

target_script = p2pkh_script(target_h160)

target_output = TxOut(amount=target_amount, script_pubkey=target_script)

transaction = tx_obj = Tx(1, [tx_in], [change_output, target_output], 0, True)

# 2: Signing the transaction
z = transaction.sig_hash(0)

private_key = PrivateKey(secret=8675309)

der = private_key.sign(z).der()

sig = der + SIGHASH_ALL.to_bytes(1, 'big')

sec = private_key.point.sec()

script_sig = Script([sig, sec])

transaction.tx_ins[0].script_sig = script_sig

print(transaction.serialize().hex())

