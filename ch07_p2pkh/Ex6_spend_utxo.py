import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from PrivateKey import PrivateKey
from Script import Script,p2pkh_script
from helper import SIGHASH_ALL, hash256, decode_base58, little_endian_to_int
from Tx import Tx, TxIn, TxOut

# Prev Txn: cda80b9607b85bcfb2e9116da57f4dc0d7a5235e65fc276930bac697d1bb5786
# source address mgWFy3V19Unq7HBwd46R8WJQgrrraxQ3cr (from ch04 - exercise 8)
# destination address: mwJn1YPMq7y5F8J3LkC5Hxg9PHyZ5K4cFv (from book exercise)

# Create 1 TxIn
# https://blockstream.info/testnet/address/mgWFy3V19Unq7HBwd46R8WJQgrrraxQ3cr
# Balance 0.001 tBTC

balance = int(.001 * 100000000)
prev_tx = bytes.fromhex('cda80b9607b85bcfb2e9116da57f4dc0d7a5235e65fc276930bac697d1bb5786')

prev_index = 1

target_address = 'mwJn1YPMq7y5F8J3LkC5Hxg9PHyZ5K4cFv'

target_amount = int(0.6 * balance)

change_address = 'mgWFy3V19Unq7HBwd46R8WJQgrrraxQ3cr'

# Pay 200 sats in fees
change_amount = balance - target_amount - 300

# Private Key - e
secret = 'shhh...it is a secret md.nauman@gmail.com!'
passpharase = secret.encode()
digest = hash256(passpharase)
little_n = little_endian_to_int(digest)
e = PrivateKey(little_n)

# Input Tx
tx_ins = []
tx_ins.append(TxIn(prev_tx, prev_index))
''' 
    tx_ins:
        [cda80b9607b85bcfb2e9116da57f4dc0d7a5235e65fc276930bac697d1bb5786:1]
'''

# Output Tx
tx_outs = []
h160 = decode_base58(target_address)
scrip_pubkey = p2pkh_script(h160)

tx_outs.append(TxOut(target_amount, scrip_pubkey))

h160 = decode_base58(change_address)
scrip_pubkey = p2pkh_script(h160)
tx_outs.append(TxOut(change_amount, scrip_pubkey))
'''
Output: 
    [   
        60000 OP_DUP OP_HASH160 ad346f8eb57dee9a37981716e498120ae80e44f7 OP_EQUALVERIFY OP_CHECKSIG,
        39700 OP_DUP OP_HASH160 0ad6763c7cc43e4d877ad821235bce765f6957cf OP_EQUALVERIFY OP_CHECKSIG
    ]
'''

tx_obj = Tx(1, tx_ins, tx_outs, 0, testnet=True)

print(tx_obj.sign_input(0, e))

print(tx_obj.serialize().hex())

'''
# Broadcast the txn
shhh...it is a secret md.nauman@gmail.com!
https://live.blockcypher.com/btc/pushtx

# Done
Tx Id: fd9cc5eab5876084146e2c6f7669e7972d691af2c5d6005629623a81c92eba0f
https://blockstream.info/testnet/tx/fd9cc5eab5876084146e2c6f7669e7972d691af2c5d6005629623a81c92eba0f



Didn't work Blockcypyer Broadcast with 200 Sat in fees
Tx Id: 27712da4745be7b427355a6d27810b10ed2d1eedf2d83dc580dea2d37bd91215
https://live.blockcypher.com/btc-testnet/tx/27712da4745be7b427355a6d27810b10ed2d1eedf2d83dc580dea2d37bd91215/
01000000018657bbd197c6ba306927fc655e23a5d7c04d7fa56d11e9b2cf5bb807960ba8cd010000006a473044022073dad9b5428479507c69f28461f690dfb226b7173067f53c23b751c5286bcdfc022077093809c0408bc6e3434086978de15f0c9a48ec509eb6c4929683d5cf0b1587012103cc14dc24187a3ba98f901e878f507bf3177005d2336857a768ddc82f9663533affffffff0260ea0000000000001976a914ad346f8eb57dee9a37981716e498120ae80e44f788ac789b0000000000001976a9140ad6763c7cc43e4d877ad821235bce765f6957cf88ac00000000
'''