from S256Field import S256Field
from Point import Point
from helper import encode_base58_checksum, hash160
# Elliptic Curve parameter
gx = 0x79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798
gy = 0x483ada7726a3c4655da4fbfc0e1108a8fd17b448a68554199c47d08ffb10d4b8
A = 0
B = 7

# Field Prime
P = 2**256 - 2**32 - 977

# Order of the Group
N = 0xfffffffffffffffffffffffffffffffebaaedce6af48a03bbfd25e8cd0364141

# Public point for the private key
class S256Point(Point):
    def __init__(self, x, y, a=None, b=None):
        a, b = S256Field(A), S256Field(B)
        
        if type(x) == int:
            super().__init__(x=S256Field(x), y=S256Field(y), a=a, b=b)
        else:
            # if x, y are points at infinity, let them 
            # pass thru directly instead of using S256Field class
            super().__init__(x=x, y=y, a=a, b=b)
            
    def __rmul__(self, coeffcient):
        
        # To make it more efficient, mod by N, because nG = 0 i.e. every N times 
        # we cycle back to zero 
        coef = coeffcient % N
        return super().__rmul__(coef)
    
    def __repr__(self):
        if self.x is None:
            return 'S256Point(infinity)'
        else:
            return f'S256Point({self.x},{self.y})'
    
    def verify(self, z, sig):
        # Calculate 1/s using Fermat's little theorem
        s_inv = pow(sig.s, N-2, N)
        u = z * s_inv % N
        v = sig.r * s_inv % N
        total = u * G + v * self
        return total.x.num == sig.r
    
    def hash160(self, compressed=True):
        return hash160(self.sec(compressed))
    
    def address(self, compressed=True, testnet=False):
        '''Returns the address string'''
        h160 = self.hash160(compressed)
        if testnet:
            prefix = b'\x6f'
        else:
            prefix = b'\x00'
        return encode_base58_checksum(prefix + h160)
    
    # SEC: Standard for Efficient Cryptography
    # Returns Compressed or Uncompressed Public Key
    def sec(self, compressed=True):
        '''returns the binary version of the SEC format'''
        # 2 or 3 ==> Compressed format of Public Key
        if compressed:
            if self.y.num % 2 == 0:
                return b'\x02' + self.x.num.to_bytes(32, 'big')
            else:
                return b'\x03' + self.x.num.to_bytes(32, 'big')
        # 4 ==> Uncompressed format of Public Key
        else:
            return b'\x04' + self.x.num.to_bytes(32, 'big') + \
                self.y.num.to_bytes(32, 'big')
    
    @classmethod
    def parse(cls, sec_bin):
        '''returns a Point object from a SEC binary (not hex)'''
        # 4 ==> Uncompressed format
        if sec_bin[0] == 4:
            x = int.from_bytes(sec_bin[1:33], 'big')
            y = int.from_bytes(sec_bin[33:65], 'big')
            return S256Point(x=x, y=y)
        
        # 2 or 3 => compressed format
        is_even = sec_bin[0] == 2
        x = S256Field(int.from_bytes(sec_bin[1:],'big'))
        
        # Right side of the equation y^2 = x^3 + 7
        alpha = x**3 + S256Field(B)
        
        # Solve for the left side
        beta = alpha.sqrt()
        
        if beta.num % 2 == 0:
            even_beta = beta
            odd_beta = S256Field(P - beta.num)
        else:
            even_beta = S256Field(P - beta.num)
            odd_beta = beta
        
        if is_even:
            return S256Point(x, even_beta)
        else:
            return S256Point(x, odd_beta)
        
        
G = S256Point(gx, gy)
