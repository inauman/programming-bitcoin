import unittest

from FieldElement import FieldElement

class Point:
    
    def __init__(self, x, y, a ,b):
        self.x = x
        self.y = y
        self.a = a
        self.b = b
        
        # Identity i.e. the Point at Infinity
        if self.x is None and self.y is None:
            return
        
        if y ** 2 != x ** 3 + a * x + b:
            raise ValueError(f'({x}, {y}) is not on the curve')
    
    def __repr__(self):
        if self.x is None:
            return 'Point(infinity)'
        elif isinstance(self.x, FieldElement):
            return f'Point({self.x.num}, {self.y.num})_{self.a.num}_{self.b.num}) FieldElement({self.x.prime})'
        else:
            return f'Point({self.x}, {self.y})_{self.a}_{self.b}'
    
    def __add__(self, other):
        if self.a != other.a or self.b != other.b:
            raise TypeError(f'Points {self}, {other} are not on the curve')
        
        '''Case # 1: Identity Point'''
        # First object is Identity i.e. the point at infinity
        if self.x is None:
            return other
        
        # If the second object is Identity i.e. the point at infinity
        if other.x is None:
            return self
        
        # Both the objects are additive inverse i.e. same x but different y
        # which would result in Identity object, i.e. the point at inifinity
        if self.x == other.x and self.y != other.y:
            return Point(None, None, self.a, self.b)
        
        '''Case # 2: x1 != x2 i.e. when the line cutting the ec across 3 points'''
        if self.x != other.x:
            # Formula to find (x3,y3)
            # slope s = (y2 - y1)/(x2 - x1)
            # x3 = s ** 2 - x1 - x2
            # y3 = s(x1 - x3) - y1
            s = (other.y - self.y)/(other.x - self.x)
            x3 = s ** 2 - self.x - other.x
            y3 = s * (self.x - x3) - self.y
            return self.__class__(x3, y3, self.a, self.b)
        
        '''Case # 3: x1 == x2 i.e. when the line is a tangent to the elliptic curve'''
        if self == other:
            # Formula
            # slope s = (3x ** 2 + a)/ (2 * y)
            # x3 = s ** 2 - 2 * x1
            # y3 = s * (x1 - x3) - y1
            s = (3 * self.x ** 2 + self.a)/(2 * self.y)
            x3 = s ** 2 - 2 * self.x
            y3 = s * (self.x - x3) - self.y
            return self.__class__(x3, y3, self.a, self.b)
        
        ''' Special case when P1 = P2 and Y is zero i.e. when the tangent is at the
        max negagtive X => slope will be None'''
        if self == other and self.y == 0 * self.x:
            # With slope None, x3 and y3 both will be None
            return self.__class__(x=None, y=None, a=self.a, b=self.b)
    
    def __rmul__(self, coefficient):
        # Scalar multiplier
        coef = coefficient
        
        # Bucket to hold the results when we are chopping off the 
        # least significant bit in every iteration
        result = Identity = self.__class__(None, None, self.a, self.b)
        
        # "current" is holding the pointer to the current bit
        current = self
        
        while coef:
            
            if coef & 1:
                # If true, trasnfer the result from "current" counter to "result" bucket
                result += current
            
            # Double the point by adding to itself
            current += current
            
            # Chop off the least significant bit
            coef >>= 1
        
        # Return the result
        return result
        
    def __eq__(self, other):
        return self.x == other.x and self.y == other.y \
            and self.a == other.a and self.b == other.b 
    
    def __ne__(self, other):
        return not (self == other)
                    
class PointTest(unittest.TestCase):
    def test_point_on_curve(self):
        p1 = Point(-1, -1, 5, 7)
        p4 = Point(-1, -1, 5, 7) 
        p5 = Point(18, 77, 5, 7) 
        
    def test_point_not_on_curve(self):
        with self.assertRaises(ValueError):
            p2 = Point(-1, -2, 5, 7)
            p4 = Point(-1, -1, 5, 7) 
            p5 = Point(18, 77, 5, 7)
          
    def test_ne(self):
        a = Point(x=3, y=-7, a=5, b=7)
        b = Point(x=18, y=77, a=5, b=7)
        self.assertTrue(a != b)
        self.assertFalse( a != a)
    
    # Identity Test Case           
    def test_add0(self):
        a = Point(x=None, y=None, a=5, b=7)
        b = Point(x=2, y=5, a=5, b=7)
        c = Point(x=2, y=-5, a=5, b=7)
        self.assertEqual(a+b, b)
        self.assertEqual(b+a, b)
        self.assertEqual(b+c, a)
    
    # Adding when x1 != x2 i.e. the two points are not on vertical line
    def test_add1(self):
        a = Point(x=3, y=7, a=5, b=7)
        b = Point(x=-1, y=-1, a=5, b=7)
        c = Point(x=2, y=-5, a =5, b=7)
        d = Point(x=2, y=5, a=5, b=7)
        e = b + d
        #print(e)
        self.assertEqual(a+b,c)

    # Adding when P1 = P2 i.e. tangent on the elliptical curve
    def test_add2(self):
        a = Point(x=-1, y=-1, a=5, b=7)
        c = Point(x=18, y=77, a=5, b=7)
        #print (a + a)
        self.assertEqual(a + a, c)
    
    def test_rmulti(self):
        x = FieldElement(47, 223)
        y = FieldElement(71, 223)
        a = FieldElement(0, 223)
        b = FieldElement(7, 223)
        p = Point(x, y, a, b)
        print(20*p)
        
if __name__ == "__main__":
    unittest.main()
