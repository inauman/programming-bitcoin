from S256Point import S256Point, G, N
from Signature import Signature
from helper import encode_base58_checksum, hash160
from random import randint
import hashlib, hmac

class PrivateKey:
    def __init__(self, secret):
        # Private Key
        self.secret = secret
        
        # Public Key
        self.point = secret * G
    
    def hex(self):
        return f'{self.secret:x}'.zfill(64)
    
    def sign(self, z):
        
        # Pick a random number from the order of the group N
        #k = randint(0, N)
        '''
        Hardcoding k for now but review and fix the deterministic_k function.
        #k = self.deterministic_k(z)
        '''
        k = self.deterministic_k(z)
        #print(k)
        #k=89603144536416929540544889335617352748312332324637366385989945150867230503155

        # Computer r of the signature(r, s)
        r = (k * G).x.num
        
        # Computer inverse of k (1/k) using Fermat's little theorem
        k_inv = pow(k, N-2, N)
        
        # Computer s of the signature(r, s)
        s = (z + r * self.secret) * k_inv % N
        
        if s > N/2:
            s = N - s
        
        return Signature(r, s)
    

    def deterministic_k(self, z):
        k = b'\x00' * 32
        v = b'\x01' * 32
        #print(f'{z}:{N} --> {z > N}')

        if z > N:
            z -= N
        z_bytes = z.to_bytes(32, 'big')
        secret_bytes = self.secret.to_bytes(32, 'big')
        s256 = hashlib.sha256
        k = hmac.new(k, v + b'\x00' + secret_bytes + z_bytes, s256).digest()
        v = hmac.new(k, v, s256).digest()
        k = hmac.new(k, v + b'\x01' + secret_bytes + z_bytes, s256).digest()
        v = hmac.new(k, v, s256).digest()
        while True:
            v = hmac.new(k, v, s256).digest()
            candidate = int.from_bytes(v, 'big')
            #print(f'candidate - {candidate} - {candidate >= 1 and candidate < N}')
            if candidate >= 1 and candidate < N:
                return candidate
            k = hmac.new(k, v + b'\x00', s256).digest()
            v = hmac.new(k, v, s256).digest()
    
    10135448583994139648504615837415888082623008770309256147192971283662981536465
    44115293411079080608845322940612473404223638699964938142319651084746813916967
    99522720241174238310570395167290887930904316983278315043471672809714401480249        
    # WIF: Wallet Import Format to serialize the private key in case 
    # private key needs to be transferred from one wallet to another
    def wif(self, compressed=True, testnet=False):
        
        # Notice big endian format
        secret_bytes = self.secret.to_bytes(32, 'big')
        
        if testnet:
            prefix = b'\xef' 
        else:
            prefix = b'\x80'
            if compressed:
                suffix = b'\x01'
            else:
                suffix = b''
        return encode_base58_checksum(prefix + secret_bytes + suffix)
