import unittest

class FieldElement:
    
    def __init__(self, num, prime):
        if num >= prime or num < 0:
            error = f'Num {num} not in field range 0 to {prime-1}'
            raise ValueError(error)
        self.num = num
        self.prime = prime
    
    def __repr__(self):
        return f'FieldElement_{self.prime}({self.num})'
    
    # Check for quality of num and prime properties of the object
    def __eq__(self, other):
        if other is None:
            return False
        return self.num == other.num and self.prime == other.prime

    def __ne__(self, other):
        if other is None:
            return False
        return self.num != other.num or self.prime != other.prime
    
    def __add__(self, other):
        if self.prime != other.prime:
            raise TypeError('Cannot add two numbers in different Fields')
        
        #Finite Field addition wraps around the prime number by using modulo
        num = (self.num + other.num) % self.prime
        return self.__class__(num, self.prime)
    
    def __sub__(self, other):
        if self.prime != other.prime:
            raise TypeError('Cannot subtract two numbers in different Fields')
        
        #Finite Field subtraction wraps around the prime number by using modulo
        num = (self.num - other.num) % self.prime
        return self.__class__(num, self.prime)

    def __mul__(self, other):
        if self.prime != other.prime:
            raise TypeError("Cannot multiply two numbers in different Fields")
        num = (self.num * other.num) % self.prime
        return self.__class__(num, self.prime)
    
    def __rmul__(self, coefficient):
        num = (self.num * coefficient) % self.prime
        return self.__class__(num=num, prime=self.prime)
    
    def __pow__(self, exponent):
        n = exponent % (self.prime - 1)
        num = pow(self.num, n, self.prime)
        return self.__class__(num, self.prime)
    
    def __truediv__(self, other):
        if self.prime != other.prime:
            raise TypeError("Cannot divide two numbers from different Fields")
        
        num = (self.num * pow(other.num, self.prime - 2, self.prime)) % self.prime
        return self.__class__(num, self.prime)
    
class FieldElementTest(unittest.TestCase):
    def test_eq(self):
        a = FieldElement(7, 13)
        b = FieldElement(7, 13)
        self.assertEqual(a, b)
    
    def test_ne(self):
        a = FieldElement(2, 31)
        b = FieldElement(2, 31)
        c = FieldElement(15, 31)
        self.assertEqual(a, b)
        self.assertTrue(a != c)
        self.assertFalse(a != b)

    def test_fa(self):
        a = FieldElement(7, 13)
        b = FieldElement(12, 13)
        c = FieldElement(6, 13)
        self.assertEqual(a+b,c)
    
    def test_pow_positive(self):
        a = FieldElement(3, 13)
        b = FieldElement(1, 13)
        self.assertEqual(a ** 3, b)
        
    def test_pow_negative(self):
        a = FieldElement(7, 13)
        b = FieldElement(8, 13)
        self.assertEqual(a ** -3, b)
        
if __name__ == '__main__':
    unittest.main()