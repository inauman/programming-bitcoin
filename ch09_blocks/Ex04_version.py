import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from io import BytesIO
from Block import Block

b = Block.parse(BytesIO(bytes.fromhex('020000208ec39428b17323fa0ddec8e887b\
4a7c53b8c0a0a220cfd0000000000000000005b0750fce0a889502d40508d39576821155e9c9e3\
f5c3157f961db38fd8b25be1e77a759e93c0118a4ffd71d')))

print(f'BIP0009: {b.version >> 29 == 0b001}')
print(f'BIP0091: {b.version >> 3 & 1 == 1}')
print(f'BIP0141: {b.version >> 1 & 1 == 1}')