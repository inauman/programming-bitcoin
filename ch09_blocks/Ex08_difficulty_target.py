import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from Block import Block
from helper import TWO_WEEKS, target_to_bits
from io import BytesIO

# Block 471744:

last_block = Block.parse(BytesIO(bytes.fromhex('000000203471101bbda3fe307664b3283a9ef0e97d9a38a7eacd88000000000000000000\
10c8aba8479bbaa5e0848152fd3c2289ca50e1c3e58c9a4faaafbdf5803c5448ddb84559\
7e8b0118e43a81d3')))


first_block = Block.parse(BytesIO(bytes.fromhex('02000020f1472d9db4b563c35f97c428ac903f23b7fc055d1cfc26000000000000000000\
b3f449fcbe1bc4cfbcb8283a0d2c037f961a3fdf2b8bedc144973735eea707e126425859\
7e8b0118e5f00474')))

time_differential = last_block.timestamp - first_block.timestamp

if time_differential > TWO_WEEKS * 4:
    time_differential = TWO_WEEKS * 4
    
if time_differential < TWO_WEEKS // 4:
    time_differential = TWO_WEEKS // 4
    
new_target = last_block.target() * time_differential // TWO_WEEKS

new_bits = target_to_bits(new_target)

print(new_bits.hex())
    
