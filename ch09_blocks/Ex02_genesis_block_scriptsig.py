import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from io import BytesIO
from Script import Script
from helper import little_endian_to_int

scriptSig = bytes.fromhex('5e03d71b07254d696e656420627920416e74506f6f6c20626a31312f4542312f4144362f43205914293101fabe6d6d678e2c8c34afc36896e7d9402824ed38e856676ee94bfdb0c6c4bcd8b2e5666a0400000000000000c7270000a5e00e00')

# get the stream
stream = BytesIO(scriptSig)

# get the commands from the scriptSig
s = Script.parse(stream)

# get the list of commands from the script
cmds = s.cmds

# 0 is the block height BIP0034 to have a unique coinbase txn in every block
print(little_endian_to_int(cmds[0]))

