import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from helper import little_endian_to_int, hash256

bits = bytes.fromhex('e93c0118')

exponent = bits[-1]

coefficient = little_endian_to_int(bits[:-1])

target = coefficient * 256**(exponent - 3)

difficulty = 0xffff * 256**(0x1d - 3) / target

print(difficulty)