import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from helper import little_endian_to_int, hash256

bits = bytes.fromhex('e93c0118')

# last bit is exponent
exponent = bits[-1]

# coefficient is the first 3 bits in LE format
coefficient = little_endian_to_int(bits[:-1])

# target = coefficient * 256**(exponent - 3)
target = coefficient * 256**(exponent - 3)
print(target)
print(f'{target:x}'.zfill(64))

'''
# A valid proof-of-work is a hash of the block header 
# below the target number
'''

block_hash = hash256(bytes.fromhex('020000208ec39428b17323fa0ddec8e887b4a7\
c53b8c0a0a220cfd0000000000000000005b0750fce0a889502d40508d39576821155e9c9e3f5c\
3157f961db38fd8b25be1e77a759e93c0118a4ffd71d'))

# proof is to LE format
proof = little_endian_to_int(block_hash)
print(proof)
print(f'{proof:x}'.zfill(64))
print(proof < target)