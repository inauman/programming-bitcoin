import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from io import BytesIO
from Script import Script
from helper import little_endian_to_int

# scriptSig of Genesis block
scriptSig = bytes.fromhex('4d04ffff001d0104455468652054696d65732030332f4a616e2f32303039204368616e63656c6c6f72206f6e206272696e6b206f66207365636f6e64206261696c6f757420666f722062616e6b73')

# get the stream
stream = BytesIO(scriptSig)

# get the commands from the scriptSig
s = Script.parse(stream)

# get the list of commands from the script
cmds = s.cmds

# 2 is can be any arbitrary message
print(s.cmds[2].decode())