import sys
sys.path.append('/Users/nauman/Projects/programming-bitcoin/')

from Block import Block
from helper import TWO_WEEKS
from io import BytesIO

last_block = Block.parse(BytesIO(bytes.fromhex('00000020fdf740b0e49cf75bb3\
d5168fb3586f7613dcc5cd89675b0100000000000000002e37b144c0baced07eb7e7b64da916cd\
3121f2427005551aeb0ec6a6402ac7d7f0e4235954d801187f5da9f5')))

first_block = Block.parse(BytesIO(bytes.fromhex('000000201ecd89664fd205a37\
566e694269ed76e425803003628ab010000000000000000bfcade29d080d9aae8fd461254b0418\
05ae442749f2a40100440fc0e3d5868e55019345954d80118a1721b2e')))

time_differential = last_block.timestamp - first_block.timestamp

if time_differential > TWO_WEEKS * 4:
    time_differential = TWO_WEEKS * 4
    
if time_differential < TWO_WEEKS // 4:
    time_differential = TWO_WEEKS // 4
    
new_target = last_block.target() * time_differential // TWO_WEEKS

print(f'{new_target:x}'.zfill(64))
